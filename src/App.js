import logo from './logo.svg';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h3>Farrid Kuntoro ✨</h3>
        <p>
          Letsgo <code>guys</code>, keep growth 🚀
        </p>
        <p>
          ini perubahan dari <code>kun-branch</code>
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn CI / CD
        </a>
      </header>
    </div>
  );
}

export default App;
